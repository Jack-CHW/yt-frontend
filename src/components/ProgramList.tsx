import React, { useState, useEffect } from "react"
import axios, { CancelToken, CancelTokenSource } from "axios"

import { ImageList, ImageListItem, Tooltip } from "@mui/material"

import config from "../config.json"
const BASEURL = config.baseUrl

interface QueryCondition {
  per_page: number
  page: number
  author: string
  album: string
  title: string
  search: string
}

interface Program {
  yt_id: string
  publish_date: string
  download_date: string
  title: string
  album: string
  author: string
  status: string
  thumbnail_url: string
}

let cancelToken: CancelTokenSource
export const ProgramList: React.FC<{ keywords: string }> = ({ keywords }) => {
  const [programs, setPrograms] = useState([] as Program[])

  const handleSearchChange = async (keywords: string) => {
    if (typeof cancelToken != typeof undefined) {
      cancelToken.cancel("Operation canceled due to new request.")
    }

    cancelToken = axios.CancelToken.source()

    return axios.get(
      BASEURL + "/api/program/?per_page=999&search=" + keywords,
      {
        cancelToken: cancelToken.token,
      }
    )
  }

  useEffect(() => {
    handleSearchChange(keywords).then((response) => {
      const programs = response.data.data as Program[]
      if (programs.length > 0) {
        setPrograms(programs)
      }
    })
  }, [keywords])
  return (
    <ImageList cols={3}>
      {programs.map((item) => (
        <Tooltip
          key={item.yt_id}
          title={item.title}
          enterDelay={500}
          leaveDelay={200}
          arrow={true}
        >
          <ImageListItem key={item.yt_id}>
            <img
              src={item.thumbnail_url}
              srcSet={item.thumbnail_url}
              alt={item.title}
              loading="lazy"
            />
          </ImageListItem>
        </Tooltip>
      ))}
    </ImageList>
  )
}
