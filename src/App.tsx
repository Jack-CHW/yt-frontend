import React, { useState } from "react"

import { Container, Box } from "@mui/material"

import { ProgramList } from "./components/ProgramList"
import { TopBar } from "./components/TopBar"

const MyHook: React.FC<{ startCount: number }> = ({ children, startCount }) => {
  const [count, setCount] = useState(startCount)
  const handleClick = () => {
    setCount(count + 1)
  }

  return (
    <>
      <button onClick={handleClick}>{count}</button>
      {count > 10 && children}
    </>
  )
}

function App() {
  const [keywords, setKeywords] = useState("")
  return (
    <>
      <TopBar setKeywords={setKeywords} />
      <Box bgcolor="background.default" mt={5}>
        <Container maxWidth="xl">
          <ProgramList keywords={keywords} />
        </Container>
      </Box>
    </>
  )
}

export default App
